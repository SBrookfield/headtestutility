using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HeadTestUtility.ExtensionMethods;
using HeadTestUtility.Models;
using Middleware;
using Models.Messages;
using Shared.Enums;
using Utils;

namespace HeadTestUtility.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Properties

        // Show some loading UI when loading.
        public bool Loading
        {
            get => _loading;
            set => Set(ref _loading, value);
        }

        // Store the selected target.
        public string SelectedTarget
        {
            get => _selectedTarget;
            set
            {
                if (!Set(ref _selectedTarget, value))
                    return;
                
                // If the target changes, updated the Move buttons.
                MoveAllCommand.RaiseCanExecuteChanged();
                MoveHeadCommand.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<string> Targets { get; set; }
        public ObservableCollection<Head> Heads { get; set; }
        public RelayCommand MoveAllCommand { get; set; }
        public RelayCommand<Head> MoveHeadCommand { get; set; }

        #endregion
        
        private bool _loading;
        private string _selectedTarget;

        public MainViewModel()
        {
            // Initialise the collections and commands.
            Targets = new ObservableCollection<string>();
            Heads = new ObservableCollection<Head>();
            MoveAllCommand = new RelayCommand(MoveAll, CanMoveAll);
            MoveHeadCommand = new RelayCommand<Head>(MoveHead, CanMoveHead);

            // Register the messages.
            RegisterMessages();

            // Load the data if we're in runtime.
            if (!IsInDesignMode)
                Load();
        }

        #region Load

        private async void Load()
        {
            Loading = true;
            await LoadAllTargets();
            await LoadAllHeads();
            Loading = false;
        }

        private async Task LoadAllTargets()
        {
            // Get all the target names.
            var targets = await MiddlewareService.HeadManager.GetAllTargetNames();
            // Add them to the observable collection.
            Targets.AddRange(targets);
        }

        private async Task LoadAllHeads()
        {
            // Get all the head names.
            var headNames = await MiddlewareService.HeadManager.GetAllHeadNames();
            // Add them to the observable collection.
            Heads.AddRange(headNames.Select(name => new Head(name)));
        }

        #endregion

        #region Command Methods

        private void MoveHead(Head head)
        {
            MiddlewareService.HeadManager.MoveHead(head.Name, SelectedTarget);
        }

        private bool CanMoveHead(Head head)
        {
            return SelectedTarget != null && head.Status == RobotStatus.Idle;
        }

        private void MoveAll()
        {
            foreach (var head in Heads)
                MoveHead(head);
        }

        private bool CanMoveAll()
        {
            return SelectedTarget != null && Heads.Any(h => h.Status == RobotStatus.Idle);
        }

        #endregion

        public void RegisterMessages()
        {
            // Register for the PositionChanged and StatusChanged messages.
            MessageBus.Current.Register<PositionChangedMessage>(UpdateHeadPosition);
            MessageBus.Current.Register<StatusChangedMessage>(UpdateHeadStatus);
        }

        private void UpdateHeadPosition(PositionChangedMessage positionChangedMessage)
        {
            // Set the TimeToShot to the specified time to shot for the specified head.
            SetHeadPropertyFromMessage(positionChangedMessage, h => h.TimeToShot, positionChangedMessage.TimeToShot);
        }

        private void UpdateHeadStatus(StatusChangedMessage statusChangedMessage)
        {
            // Set the Status to the specified status for the specified head.
            SetHeadPropertyFromMessage(statusChangedMessage, h => h.Status, statusChangedMessage.Status);
        }

        private void SetHeadPropertyFromMessage<TProperty>(HeadChangedMessage message, Expression<Func<Head, TProperty>> propertyExpression, TProperty value)
        {
            // Find the specified head.
            var head = Heads.FirstOrDefault(h => h.Name == message.Name);

            // If the head can't be found, don't panic.
            if (head == null)
                return;

            // Find the property we're meant to be setting from the expression, and set it.
            if (propertyExpression.Body is MemberExpression memberExpression)
                if(memberExpression.Member is PropertyInfo property)
                    property.SetValue(head, value, null);
              
        }
    }
}