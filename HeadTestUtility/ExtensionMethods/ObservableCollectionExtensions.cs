﻿#region Copyright

// Filename: ObservableCollectionExtensions.cs
// Date Created: 16/10/2018 23:48
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HeadTestUtility.ExtensionMethods
{
    public static class ObservableCollectionExtensions
    {
        /// <summary>
        /// Adds all items of an IEnumerable to the ObservableCollection.
        /// </summary>
        /// <typeparam name="T">The item type of both collections.</typeparam>
        /// <param name="collection">The observable collection to modify.</param>
        /// <param name="items">The items to add to the collection.</param>
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            // It's annoying that this doesn't already exist for observable collections...
            foreach (var item in items)
                collection.Add(item);
        }
    }
}