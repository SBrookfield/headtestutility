﻿#region Copyright

// Filename: Head.cs
// Date Created: 18/10/2018 00:17
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System;
using GalaSoft.MvvmLight;
using Shared.Enums;

namespace HeadTestUtility.Models
{
    public class Head : ObservableObject
    {
        public string Name
        {
            get => _name;
            set => Set(ref _name, value);
        }

        public TimeSpan TimeToShot
        {
            get => _timeToShot;
            set
            {
                // If the TimeToShot is the same, don't try to calculate anything.
                if (!Set(ref _timeToShot, value))
                    return;

                // If the provided TimeToShot is greater than the last biggest TimeToShot.
                if (value > _maxTimeToShot)
                {
                    // Replace the max TimeToShot to get a better time estimate.
                    _maxTimeToShot = value;
                    // Calculate 1% of the total milliseconds now, for use in calculating the current percentage.
                    _step = _maxTimeToShot.TotalMilliseconds / 100;
                }

                // Calculate the current progress based on the provided TimeToShot.
                Progress = 100 - (value.TotalMilliseconds / _step);
            }
        }

        public RobotStatus Status
        {
            get => _status;
            set => Set(ref _status, value);
        }

        public double Progress
        {
            get => _progress;
            set => Set(ref _progress, value);
        }

        private string _name;
        private RobotStatus _status;
        private TimeSpan _timeToShot;
        private TimeSpan _maxTimeToShot;
        private double _progress;
        private double _step;

        public Head(string name)
        {
            Name = name;
        }
    }
}