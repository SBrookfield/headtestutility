﻿using System.Windows;
using IocStore;

namespace HeadTestUtility
{
    public partial class App : Application
    {
        public App()
        {
            Ioc.Populate("BusinessLayer");
        }
    }
}