﻿namespace Interview1
{
    /// <summary>
    /// The Shot class stores the position for a camera head.
    /// </summary>
    public class Shot
    {
        /// <summary>
        /// Gets the head id for the shot.
        /// </summary>
        public string HeadId {get; private set;}

        /// <summary>
        /// Gets the position for the head shot.
        /// </summary>
        public Position Position {get; private set;}

        /// <summary>
        /// Initialises a new instance of the <see cref="Shot"/> class.
        /// </summary>
        /// <param name="headId">The id of the camera head that this shot is for.</param>
        /// <param name="position">The shot camera head position.</param>
        public Shot(string headId, Position position)
        {
            HeadId = headId;
            Position = position;
        }
    }
}
