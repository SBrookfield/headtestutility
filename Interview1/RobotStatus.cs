﻿namespace Interview1
{
    /// <summary>
    /// The status of the robot.
    /// </summary>
    public enum RobotStatus
    {
        /// <summary>
        /// Not doing anything
        /// </summary>
        Idle,
        /// <summary>
        /// Moving.
        /// </summary>
        Moving,
        /// <summary>
        /// Disabled, either due to an error or by the user.
        /// </summary>
        Disabled
    }
}
