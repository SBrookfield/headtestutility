﻿#region Copyright

// Filename: MessageBus.cs
// Date Created: 19/10/2018 20:38
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System;
using System.Collections.Generic;

namespace Utils
{
    public interface IMessageBus
    {
        /// <summary>
        /// Registers an action that will be called when a message is sent.
        /// </summary>
        /// <typeparam name="T">The message to register the action against.</typeparam>
        /// <param name="action">The action that will be called when the message is sent.</param>
        void Register<T>(Action<T> action) where T : class;

        /// <summary>
        /// Sends a message to all the registered actions.
        /// </summary>
        /// <typeparam name="T">The type of the message to send</typeparam>
        /// <param name="message">The message to send.</param>
        void Send<T>(T message) where T : class;
    }

    public class MessageBus : IMessageBus
    {
        // This uses the singleton pattern because I wanted the MessageBus to be static, but I can't put an interface on a static.
        // Because we don't want to have to keep newing up this class, using the singleton pattern means we don't have to.
        // We want to use an interface because it allows us to replace the MessageBus with a mock version if we need to TDD it.
        public static MessageBus Current => _current ?? (_current = new MessageBus());

        private static readonly Dictionary<Type, List<Action<object>>> _actionsByType;
        private static MessageBus _current;

        static MessageBus()
        {
            _actionsByType = new Dictionary<Type, List<Action<object>>>();
        }

        public void Register<T>(Action<T> action) where T : class
        {
            // Get the type of the message.
            var type = typeof(T);
            // We need to box the action otherwise the compiler gets unhappy.
            // It doesn't like us trying to store an action that returns a proper type, despite the fact it inherits from object.
            // You end up getting "cannot convert from Action<T> to Action<object>".
            var genericAction = new Action<object>(o => action((T) o));

            // If we've already got this message type, simply add the action to the list.
            // Otherwise, create a new entry for the message type, adding the action.
            if (_actionsByType.ContainsKey(type))
                _actionsByType[type].Add(genericAction);
            else
                _actionsByType.Add(type, new List<Action<object>> {genericAction});
        }

        public void Send<T>(T message) where T : class
        {
            // Get the type of the message.
            var type = typeof(T);

            // If the message isn't registered, just ignore it.
            if (!_actionsByType.ContainsKey(type))
                return;

            // If the message is registered, get all the actions we need to invoke.
            var actions = _actionsByType[type];

            // Invoke all the actions, passing in the message provided.
            foreach (var action in actions)
                action.Invoke(message);
        }
    }
}