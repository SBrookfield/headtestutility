﻿#region Copyright

// Filename: Service.cs
// Date Created: 17/10/2018 22:53
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System.Threading.Tasks;
using BusinessLayer.Managers;
using IocStore;

namespace Middleware
{
	// This represents some sort of communication framework between client and server.
    public static class MiddlewareService
    {
		public static class HeadManager
		{
			public static Task<System.Collections.Generic.List<string>> GetAllTargetNames()
			{
				return Task.Run(() => Ioc.Get<IHeadManager>().GetAllTargetNames());
			}

			public static Task<System.Collections.Generic.List<string>> GetAllHeadNames()
			{
				return Task.Run(() => Ioc.Get<IHeadManager>().GetAllHeadNames());
			}

			public static Task MoveHead(string headId, string targetName)
			{
				return Task.Run(() => Ioc.Get<IHeadManager>().MoveHead(headId, targetName));
			}
		}
	}
}