﻿<#@ template debug="true" hostspecific="false" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ assembly name="C:\Git\HeadTestUtility\BusinessLayer\bin\Debug\BusinessLayer.dll" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Reflection" #>
<#@ import namespace="BusinessLayer.Managers" #>
<#@ output extension=".cs" #>
<#
	var stringBuilder = new StringBuilder();
    var baseManagerType = typeof(BaseManager);

    var managers = Assembly
        .LoadFrom(@"C:\Git\HeadTestUtility\BusinessLayer\bin\Debug\BusinessLayer.dll")
        .GetTypes()
        .Where(t => baseManagerType.IsAssignableFrom(t) && t != baseManagerType);

    const string doubleIndent = "\t\t";
    const string tripleIndent = "\t\t\t";
    const string quadIndent = "\t\t\t\t";

    foreach (var managerType in managers)
    {
        if (stringBuilder.Length > 0)
            stringBuilder.AppendLine();

        stringBuilder.AppendLine($"{doubleIndent}public static class {managerType.Name}");
        stringBuilder.AppendLine($"{doubleIndent}{{");
		
		var firstMethod = true;

        foreach (var methodInfo in managerType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
        {
			if (!firstMethod)
				stringBuilder.AppendLine();

            var returnType = methodInfo.ReturnType;
            var parameters = methodInfo.GetParameters();

            Func<Type, string> getTypeCode = t => new Microsoft.CSharp.CSharpCodeProvider().GetTypeOutput(new System.CodeDom.CodeTypeReference(t));

            var returnTypeCode = getTypeCode(methodInfo.ReturnType);
            var genericTag = returnType == typeof(void) ? string.Empty : $"<{returnTypeCode}>";
			var methodParameters = string.Empty;
			var invokeParameters = string.Empty;

			foreach(var parameter in parameters)
			{
				methodParameters += $", {getTypeCode(parameter.ParameterType)} {parameter.Name}";
				
				if (parameter.IsOptional)
					methodParameters += $" = {(parameter.ParameterType.IsEnum ? $"{getTypeCode(parameter.ParameterType)}.{parameter.DefaultValue}" : (parameter.DefaultValue ?? "null").ToString().ToLower())}";

				invokeParameters += $", {parameter.Name}";
			}

			if (methodParameters != string.Empty)
				methodParameters = methodParameters.Remove(0, 2);

			if (invokeParameters != string.Empty)
				invokeParameters = invokeParameters.Remove(0, 2);

            stringBuilder.AppendLine($"{tripleIndent}public static Task{genericTag} {methodInfo.Name}({methodParameters})");
            stringBuilder.AppendLine($"{tripleIndent}{{");
            stringBuilder.AppendLine($"{quadIndent}return Task.Run(() => Ioc.Get<I{managerType.Name}>().{methodInfo.Name}({invokeParameters}));");
            stringBuilder.AppendLine($"{tripleIndent}}}");

			firstMethod = false;
        }
        
		stringBuilder.AppendLine($"{doubleIndent}}}");
    }
#>
#region Copyright

// Filename: Service.cs
// Date Created: 17/10/2018 22:53
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System.Threading.Tasks;
using BusinessLayer.Managers;
using IocStore;

namespace Middleware
{
	// This represents some sort of communication framework between client and server.
    public static class MiddlewareService
    {
<#= stringBuilder.ToString() #>	}
}