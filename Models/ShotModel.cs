﻿#region Copyright

// Filename: ShotModel.cs
// Date Created: 17/10/2018 00:15
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

namespace Models
{
    public class ShotModel
    {
        public string HeadId { get; set; }
        public PositionModel Position { get; set; }
    }
}