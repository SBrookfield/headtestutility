﻿#region Copyright

// Filename: PositionModel.cs
// Date Created: 17/10/2018 00:16
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

namespace Models
{
    public class PositionModel
    {
        public double Pan { get; set; }
        public double Tilt { get; set; }
    }
}