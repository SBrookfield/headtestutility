﻿#region Copyright

// Filename: HeadChangedMessage.cs
// Date Created: 21/10/2018 21:26
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

namespace Models.Messages
{
    public class HeadChangedMessage
    {
        public string Name { get; set; }
    }
}