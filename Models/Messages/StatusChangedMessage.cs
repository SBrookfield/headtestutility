﻿#region Copyright

// Filename: StatusChangedMessage.cs
// Date Created: 19/10/2018 21:07
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using Shared.Enums;

namespace Models.Messages
{
    public class StatusChangedMessage : HeadChangedMessage
    {
        public RobotStatus Status { get; }

        public StatusChangedMessage(string name, RobotStatus status)
        {
            Name = name;
            Status = status;
        }
    }
}