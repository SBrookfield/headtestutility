﻿#region Copyright

// Filename: PositionChangedMessage.cs
// Date Created: 19/10/2018 21:03
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System;

namespace Models.Messages
{
    public class PositionChangedMessage : HeadChangedMessage
    {
        public PositionModel Position { get; }
        public TimeSpan TimeToShot { get; }

        public PositionChangedMessage(string name, PositionModel position, TimeSpan timeToShot)
        {
            Name = name;
            Position = position;
            TimeToShot = timeToShot;
        }
    }
}