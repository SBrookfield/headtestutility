﻿#region Copyright

// Filename: HeadManager.cs
// Date Created: 17/10/2018 23:49
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Factories;
using Interview1;
using IocStore;
using IocStore.Attributes;
using Models.Messages;
using Utils;
using RobotStatus = Shared.Enums.RobotStatus;

namespace BusinessLayer.Managers
{
    [IocLoad]
    public interface IHeadManager
    {
        /// <summary>
        /// Get all the names of the targets from the studio.
        /// </summary>
        /// <returns>List of target names.</returns>
        List<string> GetAllTargetNames();

        /// <summary>
        /// Get all the names of the heads from the studio.
        /// </summary>
        /// <returns>List of head names.</returns>
        List<string> GetAllHeadNames();

        /// <summary>
        /// Moves the specified head to the specified target location.
        /// </summary>
        /// <param name="headId">The name of the head to move.</param>
        /// <param name="targetName">The name of the target to move the head to.</param>
        void MoveHead(string headId, string targetName);
    }

    public class HeadManager : BaseManager, IHeadManager
    {
        public List<string> GetAllTargetNames()
        {
            return Ioc.Get<IStudio>()
                .Targets
                .Select(t => t.Name)
                .ToList();
        }

        public List<string> GetAllHeadNames()
        {
            return Ioc.Get<IStudio>()
                .Robots
                .Select(r => r.Name)
                .ToList();
        }

        public void MoveHead(string headId, string targetName)
        {
            // Get the studio.
            var studio = Ioc.Get<IStudio>();

            // Get the robot for the specified name.
            var robot = studio
                .Robots
                .FirstOrDefault(r => r.Name == headId);

            // Get the target for the specified name.
            var target = studio.Targets.FirstOrDefault(r => r.Name == targetName);

            // If we can't find the target or head, exception.
            if (robot == null || target == null)
                throw new NotImplementedException();

            // Set up the robot to send a message when the position and/or status changes.
            robot.OnPositionChanged += (sender, args) => MessageBus.Current.Send(new PositionChangedMessage(((IRobot) sender).Name, args.CurrentPosition.ToModel(), args.TimeToShot));
            robot.OnStatusChanged += (sender, args) => MessageBus.Current.Send(new StatusChangedMessage(((IRobot) sender).Name, (RobotStatus) (int) args.Status));

            // Get the shot for the specified head.
            var shot = target.GetShotForCamera(headId);

            // If there is no shot, then return as there's no need to move the head.
            if (shot == null)
                return;

            // Send the robot to the specified position.
            robot.SetPosition(new Position(shot.Position.Pan, shot.Position.Tilt));
        }
    }
}