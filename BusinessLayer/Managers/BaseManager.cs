﻿#region Copyright

// Filename: BaseManager.cs
// Date Created: 17/10/2018 23:49
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

namespace BusinessLayer.Managers
{
    // Used for storing any shared manager logic.
    public class BaseManager
    {
    }
}