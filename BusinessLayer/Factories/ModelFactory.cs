﻿#region Copyright

// Filename: ModelFactory.cs
// Date Created: 17/10/2018 00:14
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using Interview1;
using Models;

namespace BusinessLayer.Factories
{
    public static class ModelFactory
    {
        // This method allows for the the conversion of the Position model to a simple POCO.
        // Since this Position model doesn't sit within the models project, it is treated as an object that might turn complex.
        public static PositionModel ToModel(this Position position)
        {
            return new PositionModel
            {
                Pan = position.Pan,
                Tilt = position.Tilt
            };
        }
    }
}