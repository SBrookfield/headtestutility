﻿#region Copyright

// Filename: Studio.cs
// Date Created: 17/10/2018 00:05
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

using System.Collections.Generic;
using Interview1;
using IocStore.Attributes;
using InterviewStudio = Interview1.Studio;

namespace BusinessLayer
{
    [IocLoad]
    public interface IStudio
    {
        IEnumerable<Target> Targets { get; }
        IEnumerable<IRobot> Robots { get; }
    }

    // Wrap the existing studio so that we can mock it.
    public class Studio : IStudio
    {
        private static readonly InterviewStudio _studio = new InterviewStudio();

        public IEnumerable<Target> Targets => _studio.Targets;
        public IEnumerable<IRobot> Robots => _studio.Robots;
    }
}