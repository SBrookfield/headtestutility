﻿#region Copyright

// Filename: IocTests.cs
// Date Created: 16/10/2018 23:48
// Copyright (c) 2018 Sebastian Brookfield http://sbrookfield.co.uk

#endregion

using System;
using AutoFixture;
using IocStore.Exceptions;
using IocStore.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace IocStore.Tests
{
    [TestClass]
    public class IocTests
    {
        private Fixture _autoFixture;
        private string _testObjectName;

        [TestInitialize]
        public void SetUp()
        {
            _autoFixture = new Fixture();

            Ioc.Clear();
            Ioc.Populate("IocStore.Tests");
        }

        [TestMethod]
        public void IocExceptionsIfNonInterfaceIsRegistered()
        {
            Assert.ThrowsException<TypeNotInterfaceException>(() => Ioc.Register<string>(string.Empty));
        }

        [TestMethod]
        public void IocExceptionsIfInterfaceTypeIsNotAssignableFromInstance()
        {
            Assert.ThrowsException<InterfaceNotAssignableFromInstanceException>(() => Ioc.Register<IIocTestObject>(string.Empty));
        }

        [TestMethod]
        public void IocExceptionsIfNonInterfaceIsGot()
        {
            Assert.ThrowsException<TypeNotInterfaceException>(Ioc.Get<string>);
        }

        [TestMethod]
        public void IocReturnsMockVersionInTests()
        {
            Ioc.Register<IIocTestObject>(GenerateMockTestObject());
            var testObject = Ioc.Get<IIocTestObject>();
            Assert.AreEqual(_testObjectName, testObject.Name);
        }

        [TestMethod]
        public void IocCallsDisposeOnClear()
        {
            var mockIocTestObject = new MockDisposableIocTestObject();
            Ioc.Register<IDisposableIocTestObject>(mockIocTestObject);
            Ioc.Clear();
            Assert.IsTrue(mockIocTestObject.DisposeCalled);
        }

        [TestMethod]
        public void IocDoesNotPopulateMultipleTimesIfAssemblyAlreadyLoaded()
        {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void IocPopulatesCorrectAssembly()
        {
            Assert.Inconclusive();
        }

        private IIocTestObject GenerateMockTestObject()
        {
            _testObjectName = _autoFixture.Create<string>();

            var mockTestObject = MockRepository.GenerateMock<IIocTestObject>();
            mockTestObject.Stub(o => o.Name).WhenCalled(mi => mi.ReturnValue = _testObjectName).Return(null);
            
            return mockTestObject;
        }
    }
}