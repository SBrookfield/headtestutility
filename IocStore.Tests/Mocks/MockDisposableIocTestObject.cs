﻿#region Copyright

// Filename: MockDisposableIocTestObject.cs
// Date Created: 16/10/2018 23:48
// Copyright (c) 2018 Sebastian Brookfield http://sbrookfield.co.uk

#endregion

using System;
using IocStore.Attributes;

namespace IocStore.Tests.Mocks
{
    [IocLoad]
    public interface IDisposableIocTestObject : IDisposable
    {
        string Name { get; }
    }

    public class MockDisposableIocTestObject : IDisposableIocTestObject
    {
        public bool DisposeCalled { get; private set; }

        public string Name { get; }

        public void Dispose()
        {
            DisposeCalled = true;
        }
    }
}