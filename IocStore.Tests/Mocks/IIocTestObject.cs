﻿#region Copyright

// Filename: IIocTestObject.cs
// Date Created: 16/10/2018 23:48
// Copyright (c) 2018 Sebastian Brookfield http://sbrookfield.co.uk

#endregion

using IocStore.Attributes;

namespace IocStore.Tests.Mocks
{
    [IocLoad]
    public interface IIocTestObject
    {
        string Name { get; }
    }
}