﻿#region Copyright

// Filename: RobotStatus.cs
// Date Created: 17/10/2018 00:35
// Copyright (c) 2018 Vitec Production Solutions Ltd http://www.vitecgroup.com

#endregion

namespace Shared.Enums
{
    // A copy of the enum so I don't need to reference the Interview project in more than the business layer.
    public enum RobotStatus
    {
        Idle,
        Moving,
        Disabled
    }
}