﻿#region Copyright

// Filename: TypeNotFoundException.cs
// Date Created: 16/10/2018 23:48
// Copyright (c) 2018 Sebastian Brookfield http://sbrookfield.co.uk

#endregion

using System;

namespace IocStore.Exceptions
{
    public class TypeNotFoundException : Exception
    {
        public TypeNotFoundException(string typeName) : base(typeName)
        {
        }
    }
}