﻿#region Copyright

// Filename: Ioc.cs
// Date Created: 16/10/2018 23:48
// Copyright (c) 2018 Sebastian Brookfield http://sbrookfield.co.uk

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using IocStore.Attributes;
using IocStore.Exceptions;

namespace IocStore
{
    public static class Ioc
    {
        //  Used to hold the instance and type of the class to return.
        private class IocObject
        {
            // Store the type of the class that inherits the interface so we can create an instance from it.
            internal Type ConcreteType { get; set; }
            // Store the instance of the concrete type.
            internal object Instance { get; set; }

            internal IocObject(Type concreteType, object instance)
            {
                ConcreteType = concreteType;
                Instance = instance;
            }
        }

        private static readonly Dictionary<Type, IocObject> _instanceByInterfaceType;
        private static readonly List<string> _loadedAssemblies;

        static Ioc()
        {
            _instanceByInterfaceType = new Dictionary<Type, IocObject>();
            _loadedAssemblies = new List<string>();
        }

        /// <summary>
        /// Loads all the interfaces from the specified assemblies where they have the IocLoad attribute.
        /// </summary>
        /// <param name="assembliesToLoad">The assemblies to load.</param>
        public static void Populate(params string[] assembliesToLoad)
        {
            // Keep track of the assemblies that are already loaded so we don't reload them.
            var alreadyLoadedAssemblies = assembliesToLoad.Intersect(_loadedAssemblies);

            // Loop through all the assemblies that we've been told to load, but aren't already loaded.
            foreach (var assemblyToLoad in assembliesToLoad.Except(alreadyLoadedAssemblies))
            {
                // Get all the types of the assembly.
                var types = Assembly.Load(assemblyToLoad).GetTypes();
                // Find all the interfaces which use the IocLoadAttribute as we need to load these.
                var interfacesToBeLoaded = types.Where(t => t.IsInterface && t.GetCustomAttribute<IocLoadAttribute>() != null);

                foreach (var interfaceType in interfacesToBeLoaded)
                {
                    // Get all the classes which are using an IocLoad interface.
                    var classes = types.Where(t => interfaceType.IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract).ToArray();

                    // If there are more than one, we can't support it as we won't know which class to load.
                    if (classes.Length > 1)
                    {
                        Debug.Fail($"The interface {interfaceType.Name} is inherited in more than one class.");
                        continue;
                    }

                    // If the interface doesn't already exist in the dictionary, add it, storing the concrete type as well.
                    if (!_instanceByInterfaceType.ContainsKey(interfaceType))
                        _instanceByInterfaceType.Add(interfaceType, new IocObject(classes.FirstOrDefault(), null));
                }

                // Now the assembly is loaded, add it to the list of loaded assemblies.
                _loadedAssemblies.Add(assemblyToLoad);
            }
        }

        /// <summary>
        /// Gets the instance of the concrete class inheriting the provided interface.
        /// </summary>
        /// <typeparam name="TInterface">The interface of the class to load.</typeparam>
        /// <returns>An instance of the inheriting class.</returns>
        public static TInterface Get<TInterface>() where TInterface : class
        {
            // Get the IocObject for the specified interface.
            // This also outputs the type of the interface, but we don't care in this instance.
            var iocObject = GetIocObjectForInterface<TInterface>(out _);

            // Lazy load the concrete class.
            // If we've not already got an instance, create one, or return the one we've already got.
            if (iocObject.Instance == null)
                iocObject.Instance = Activator.CreateInstance(iocObject.ConcreteType);

            return (TInterface) iocObject.Instance;
        }

        /// <summary>
        /// Clears the IOC store of all instances. This is used for clearing the real instances, and allows you to use a mock in its place.
        /// This may actually is actually redundant. Everything is lazy loaded, and Register will override any value that exists.
        /// If we call the IOC after a clear, and we've not put a mock in place, it'll go ahead and create a new real instance.
        /// TODO: Come back and sort this out, so that if anything, Register calls dispose before clearing an instance.
        /// </summary>
        public static void Clear()
        {
            // Get the IDisposable type.
            var iDisposableType = typeof(IDisposable);

            // For all the IOC objects we have...
            foreach (var iocObject in _instanceByInterfaceType.Values)
            {
                // If there is not instance, don't try to clear it.
                if (iocObject.Instance == null)
                    continue;

                // If the concrete class inherits from IDisposable, call dispose on it before clearing it.
                if (iDisposableType.IsAssignableFrom(iocObject.ConcreteType))
                    ((IDisposable) iocObject.Instance).Dispose();

                // Clear the instance.
                iocObject.Instance = null;
            }
        }

        /// <summary>
        /// Used to register an instance of a type.
        /// This is used for TDD so that a mock object can be returned instead of a real instance.
        /// </summary>
        /// <typeparam name="TInterface">The interface of the object to register.</typeparam>
        /// <param name="instance">The instance that should be returned when the interface is requested.</param>
        public static void Register<TInterface>(object instance) where TInterface : class
        {
            // Get the IocObject for the specified interface.
            var iocObject = GetIocObjectForInterface<TInterface>(out var interfaceType);
            // Get the type of the instance.
            var instanceType = instance.GetType();

            // Check to see if the provided class actually inherits the provided interface.
            if (!interfaceType.IsAssignableFrom(instanceType))
                throw new InterfaceNotAssignableFromInstanceException();

            // Save the concrete type, and the instance.
            iocObject.ConcreteType = instanceType;
            iocObject.Instance = instance;
        }

        private static IocObject GetIocObjectForInterface<TInterface>(out Type type) where TInterface : class
        {
            // Get the type of the interface we need to load.
            type = typeof(TInterface);

            // If this type is not an interface, then error as it's not supported.
            // Sadly there's no constraint that allows you to limit by being an interface.
            if (!type.IsInterface)
                throw new TypeNotInterfaceException(type.Name);

            // If we can't find the requested interface in our dictionary of interfaces, error.
            if (!_instanceByInterfaceType.TryGetValue(type, out var iocObject))
                throw new TypeNotFoundException(type.Name);

            return iocObject;
        }
    }
}